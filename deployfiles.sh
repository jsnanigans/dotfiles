
BASEDIR=$(dirname "$0")
BACKUPSDIR="localbackup"
FILELIST="files.txt"

if [ ! -d "$BASEDIR/$BACKUPSDIR" ]; then
    mkdir "$BASEDIR/$BACKUPSDIR"
fi

BACKUPDIRNAME="$(date +"%d-%m-%Y-%H-%M-%S")"
INSTANCEDIR="$BASEDIR/$BACKUPSDIR/$BACKUPDIRNAME"

while IFS=, read -r entry
do
    echo "Backing up \"$entry\""
    cp "$HOME/$entry" "$INSTANCEDIR"
done < "$BASEDIR/$FILELIST"

tar -cf "$BASEDIR/$BACKUPSDIR/$BACKUPDIRNAME.tar" "$BASEDIR/$BACKUPSDIR/$BACKUPDIRNAME"
echo "Backup created at \"$BASEDIR/$BACKUPSDIR/$BACKUPDIRNAME.tar\" \n"

rm -rf "$BASEDIR/$BACKUPSDIR/$BACKUPDIRNAME"


while IFS=, read -r entry
do
    echo "Copying \"$BASEDIR/files/$entry\" to \"$HOME/$entry\""
    cp "$BASEDIR/files/$entry" "$HOME/$entry"
done < "$BASEDIR/$FILELIST"


