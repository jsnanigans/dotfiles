#!/bin/bash

TMPBG=/tmp/screen.png
scrot $TMPBG

ICON=~/.config/i3/lock.png
convert $TMPBG -scale 20% -scale 500% $TMPBG
convert $TMPBG $ICON -gravity center -composite -matte $TMPBG

i3lock -i $TMPBG
