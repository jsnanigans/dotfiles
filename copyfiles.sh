BASEDIR=$(dirname "$0")

echo "Cloning files into $BASEDIR/files"

while IFS=, read -r entry
do
    if [ -f "$HOME/$entry" ]; then
        echo "Copying $entry"
        cp "$HOME/$entry" "$BASEDIR/files"
    else
        echo "ERROR: \"$HOME/$entry\" is not a file"
    fi
done < "$BASEDIR/files.txt"
